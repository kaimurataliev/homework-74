const express = require('express');
const router = express.Router();
const fs = require('fs');

router.get('/messages', (req, res) => {
    try {
        const files = fs.readdirSync('./data');
        const count = files.length < 5 ? files.length : 5;
        const messages = [];
        files.reverse();

        for (let i = 0; i < count; i++) {
            const content = fs.readFileSync(`./data/${files[i]}`);
            messages.push(JSON.parse(content));
        }
        res.send(messages);
    } catch (e) {
        console.error(e);
        res.send({error: e.message});
    }
});


router.post('/messages', (req, res) => {
    try {
        const response = {
            message: req.body.message,
            datetime: new Date().toISOString()
        };

        fs.writeFileSync(`./data/${response.datetime}.txt`, JSON.stringify(response));

        res.send(response);
    } catch (e) {
        console.error(e);
        res.send({error: e.message});
    }
});

module.exports = router;