const express = require('express');
const fs = require('fs');
const router = require('./messages');
const bodyParser = require('body-parser');


const port = 7000;
const app = express();

app.use(bodyParser.json());

app.use(router);


app.listen(port, () => {
    console.log(`sever is running on ${port} port`);
});